Minetest Game mod: Treasures
==========================
See license.txt for license information.

Outsource of treasure nodes from minerdream.

Authors of source code
----------------------
davieddoesminetest (MIT)
ademant (MIT)

Authors of media (textures)
---------------------------
  
Created by ademant (CC BY 3.0):
